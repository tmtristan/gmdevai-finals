﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject[] EnemyPrefabs;
    public EnemyManager[] Enemies;    
    public List<Transform> wayPointsForAI;

    void Start () {
        SpawnAllEnemies();
	}
	
	private void SpawnAllEnemies()
    {
        for(int i = 0; i < Enemies.Length; i++)
        {
            Enemies[i].Instance = Instantiate(EnemyPrefabs[i], Enemies[i].SpawnPoint.position, Enemies[i].SpawnPoint.rotation) as GameObject;
            Enemies[i].SetupAI(wayPointsForAI);
        }        
    }
}
