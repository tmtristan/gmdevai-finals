﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall")
        {
            Destroy(gameObject);
        }
        if (tag == "Shooter" && other.tag == "Player")
        {
            //Destroy(other.gameObject);
            other.gameObject.SetActive(false);
            Destroy(gameObject);
        }
        if (other.tag == "Shooter" && tag == "PlayerBullet")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
