﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[Serializable]
public class EnemyManager{

    public Transform SpawnPoint;                          // The position and direction the tank will have when it spawns.
    [HideInInspector]
    public GameObject Instance;         // A reference to the instance of the tank when it is created.
    [HideInInspector]
    public List<Transform> WayPointList;

    private ShootScript Shooting;                        // Reference to tank's shooting script, used to disable and enable control.
    private GameObject CanvasGameObject;                  // Used to disable the world space UI during the Starting and Ending phases of each round.
    private StateController StateController;              // Reference to the StateController for AI tanks

    public void SetupAI(List<Transform> wayPointList)
    {
        StateController = Instance.GetComponent<StateController>();
        StateController.SetupAI(true, wayPointList);

        Shooting = Instance.GetComponent<ShootScript>();

        /*if i want to color them programmatically...
        MeshRenderer[] renderers = Instance.GetComponentsInChildren<MeshRenderer>();

        for(int i = 0; i < renderers.Length; i++)
        {

        }
        */
    }
}
