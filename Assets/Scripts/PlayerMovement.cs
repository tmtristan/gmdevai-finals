﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float Speed;
   
    private Rigidbody rb;
    private ShootScript shootScript;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        shootScript = GetComponent<ShootScript>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            shootScript.Fire();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        Debug.Log(movement);

        if(movement != Vector3.zero)
        {
            var rotation = transform.rotation;
            rotation.SetLookRotation(movement);
            rb.rotation = rotation;
        }
        
        rb.velocity = movement * Speed;

    }
}
