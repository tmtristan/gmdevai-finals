﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootScript : MonoBehaviour {

    public GameObject Shot;
    public Transform ShotSpawn;
    public float FireRate;

    private float nextFire;

    void Update () {
    }

    public void Fire()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + FireRate;
            Instantiate(Shot, ShotSpawn.position, ShotSpawn.rotation);
        }
    }

    public void Fire(float fireRate)
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(Shot, ShotSpawn.position, ShotSpawn.rotation);
        }
    }
}
